# bbai64-humble-ros2

Build ROS2-Humble for BeagleBone AI-64 bullseye

Scripts for build ROS2 to running on BBAI64 (64bit).

## Requirements

- Jetson AGX Xavier or Orin (aarch64) or AWS Ubuntu-arm64

## build ROS2-base

```bash
git clone https://gitlab.com/semi-autonomous-vehicle/fart/bbai64-humble-ros2.git
cd bbai64-humble-ros2/ros2_ws

bash build.bash humble
```
After all builds are successful

```bash
ls
# > humble-aarch64.zip
```

<br>

## Install ROS2 to BBAI64-Bullseye (aarch64)

```bash
# Raspbian-Bullseye
sudo mkdir /opt/ros -p
sudo unzip humble-aarch64.zip -d /opt/ros
```


